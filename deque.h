
template<typename T>
class MyDeque {
	private:
		T* _b;
		T* _e;

	public:
		MyDeque(int size = 0, T& v = T()) {
			_b = new T[size];
			_e = _b + size;
			fill(_b, _e, v);
		}

		~MyDeque() {
			delete [] _b;
		}
}
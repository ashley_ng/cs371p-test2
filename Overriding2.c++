#include <cassert>  // assert
#include <iostream> // cout, endl
#include <string>   // string, ==


using namespace std;

struct A {
	virtual ~A() {cout << "~A()" << endl;}

	// A() {
	// 	cout << "A()" << endl;
	// }

	virtual string f(int) {
		return "A::f(int)";
	}

	virtual string g(int) {
		return "A::g(int)";
	}

	virtual string h(int) {
		return "A::h(int)";
	}
};

struct B : A {
	// B() {
	// 	cout << "B()" << endl;
	// }

	string f(int) {
		return "B::f(int)";
	}
	string g(double) {
		return "B::g(double)";
	}

	string h(int) const {
		return "B::h(int) const";
	}
};

int main() {
	cout << "Overriding2.c++" << endl;

	A *const p = new B(); // A() B()

	cout << p->f(2) << endl; // B::f(int)
	cout << p->g(2) << endl; // A::g(int)
	cout << p->g(2.0) << endl; //A::g(int)
	cout << p->h(2) << endl; //A::h(int)

	cout << "Done." << endl;
	return 0;
}
#include <algorithm> // copy, count, equal, fill
#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <iostream>  // cout, endl
#include <string>    // string
#include <vector>    // vector

using namespace std;

struct A {
	A() {
		cout << "A::A()" << endl;
	}
	~A() {
		cout << "~A::A()" << endl;
	}
	A(A&) {
		cout << "A::A(A)" << endl;
	}
	A& operator=(A) {
		cout << "=A" << endl;
		return *this;
	}
};

struct B : A {
	B() {
		cout << "B::B()" << endl;
	}
	~B() {
		cout << "~B::B()" << endl;
	}
	B(B&) {
		cout << "B::B(B)" << endl;
	}
	B& operator=(B) {
		cout << "=B" << endl;
		return *this;
	}
};

int main() {
	A *a = new B(); ///A::A() B::B()
	// A a; /// A::A() ~A::A()
	// B b; /// A::A() B::B() ~B::B() ~A::A()

	// A a[3]; /// calls constructor/destructor 3x
	// A* a = new A[3];

	return 0;
}
#include <iostream>  // cout, endl

using namespace std;

struct A {
	A() {cout << "A::A()" << endl;}
	void f() {cout << "A::f()" << endl;}
};

struct B : A {
	B() {cout << "B::B()" << endl;}
	virtual void f() {cout << "B::f()" << endl;}
};

struct X : B {
	X() {cout << "X::X()" << endl;}
	void f() {cout << "X::f()" << endl;}
};

struct C : B {
	C() {cout << "C::C()" << endl;}
	void f() {cout << "C::f()" << endl;}
};

int main() {

	A* p = new X; /// A::A() B::B() X::X()
	p->f(); /// A::f()
	B* q = new X; /// A::A() B::B() X::X()
	q->f(); /// X::f();

	return 0;
}

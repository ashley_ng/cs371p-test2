#include <algorithm> // copy, count, equal, fill
#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <iostream>  // cout, endl
#include <string>    // string
#include <vector>    // vector

using namespace std;

struct B {
	B() {
		cout << "B::B()" << endl;
	}
	~B() {
		cout << "~B::B()" << endl;
	}
	B(B&) {
		cout << "B::B(B)" << endl;
	}
	B& operator=(B) {
		cout << "=B" << endl;
		return *this;
	}
};

struct A {
	B _b;
	A() {
		cout << "A::A()" << endl;
	}
	~A() {
		cout << "~A::A()" << endl;
	}
	A(A&) {
		cout << "A::A(A)" << endl;
	}
	A& operator=(A) {
		cout << "=A" << endl;
		return *this;
	}
};

int main() {
	A x; /// B::B() A::A()

	// A y = x; /// B::B() A::A(A)
	A z;
	x = z; /// A::A(A) =A

	return 0;
}
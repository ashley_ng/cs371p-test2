#include <cassert>  // assert
#include <iostream> // cout, endl
#include <string>   // string, ==


using namespace std;

struct A {
	string f(int) {
		return "A::f(int)";
	}

	string g(int) {
		return "A::g(int)";
	}
};

struct B : A {
	string f(string) {
		return "B::f(string)";
	}
	string g(double) {
		return "B::g(double)";
	}
};

struct C : A {
	using A::f;
	using A::g;

	string f(string) {
		return "C::f(string)";
	}

	string g(double) {
		return "C::g(double)";
	}
};

int main() {
	cout << "Overriding1.c++" << endl;

	B b;

	// cout << b.f("string") << endl; // B::f(string)
	// cout << b.A::f(2) << endl; // A::f(int)
	// cout << b.g(2) << endl; // B::g(double)
	// cout << b.A::g(2) << endl; // A::g(int)
 
 	C c;
 	// cout << c.f(2) << endl; // A::f(int)
 	// cout << c.f("hi") << endl; // C::f(string)
 	cout << c.g(2) << endl; // A::g(int) can get C::g(double) to print by commenting out using

 	// A* a = new C;
 	// cout << a->f(2) << endl; // A::f(int)
 	// cout << a->f("hi") << endl; // doesn't compile

	cout << "Done." << endl;
	return 0;
}
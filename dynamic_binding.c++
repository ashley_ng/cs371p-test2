#include <iostream>  // cout, endl

using namespace std;

struct A {
	virtual void f() {cout << "A::f()" << endl;}
};

struct B : A {
	void f() {cout << "B::f()" << endl;}
};

struct C : A {
	void f() {cout << "C::f()" << endl;}
};

int main() {

	A* p;
	if (false)
		p = new B;
	else
		p = new C;

	p->f(); /// either B::f() or C::f()

	return 0;
}

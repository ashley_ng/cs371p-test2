Definition

Overload a function
	- have 2 versions of a function.
	- have different number or differnt types of arguments


Vector
	- add to front: linear
	- add to back: constant
		- amortize constant growth?


Deque
	- middle load
	- amortize for front and back


List
	- constant effort for front and back

|       | Vector | Deque | List |
|-------|--------|-------|------|
| Stack | YES | YES* | YES |
| Queue | NO | YES* | YES |
| Priority Queue | YES* | YES | NO |

* default container


Queue needs front, but vector doesn't have it
Priority Queue needs random access, list doesn't have it

Class
	- defaults to private

Struct
	- defaults to public
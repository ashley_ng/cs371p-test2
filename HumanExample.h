#include <string> //string
#include <iostream> //cout, endl;

using namespace std;

class AbstractHuman {
	protected:
		string _name;
		int _age;
	public:
		virtual ~AbstractHuman() {} 

		AbstractHuman(string name = "N/A", int age = 0) {
			_name = name;
			_age = age;
		}

		virtual AbstractHuman* clone() = 0;

};


class Woman : AbstractHuman {
	bool _is_woman;

	Woman(string name, int age) : AbstractHuman(name, age) {
		_is_woman = true;
	}

	Woman* clone() {
		return new Woman(*this);
	}

};

class Man :AbstractHuman {
	bool _is_man;

	Man(string name, int age) : AbstractHuman(name, age) {
		_is_man = true;
	}

	Man* clone() {
		return new Man(*this);
	}
};

class HumanHandler {
	AbstractHuman* _h;

	HumanHandler(AbstractHuman* h) {
		_h = h;
	}

	HumanHandler(HumanHandler& hh) {
		_h = hh._h->clone();
	}

	HumanHandler& operator=(HumanHandler& rhs) {
		delete _h;
		_h = rhs._h->clone();
		return *this;
	}

	~HumanHandler() {
		delete _h;
	}


};



#include <algorithm> // equal, lexicographical_compare
#include <cstddef>   // ptrdiff_t, size_t
#include <memory>    // allocator
#include <stdexcept> // out_of_range
#include <utility>   // !=, <=, >, >=
#include <deque> // vector

//empty
//size
//front
//back
//push
//pop
//FIFO

using namespace std;

template <typename T, template Container = deque<T> >
class MyQueue {
	Container _c;

	MyQueue(const Container& c = Container()) : _c(c) {}

	bool empty() {
		return _c.empty();
	}

	T& front() {
		return _c.front();
	}

	T& back() {
		return _c.back();
	}

	void push(T& v) {
		_c.push_back(v);
	}

	void pop() {
		_c.pop_font();
	}

	int size() {
		return _c.size();
	}
};
#include <iostream>  // cout, endl

using namespace std;

/*
	if 
		struct A
		struct B:A
	1. change behavior or B: change B
	2. change behavior of A and B: change A
	3. change behavior of only A: can't
*/

struct A {
	virtual void f(long) {cout << "A::f(long)" << endl;}
};

struct B : A {
	void f(int) {cout << "B::f(int)" << endl;}
};


int main() {

	// A* p = new B;
	// p->f(2); /// A::f(long)

	B* p = new B;
	p->f(2.0); /// B::f(int)
	

	return 0;
}

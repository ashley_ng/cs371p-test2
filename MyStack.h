#include <algorithm> // equal, lexicographical_compare
#include <cstddef>   // ptrdiff_t, size_t
#include <memory>    // allocator
#include <stdexcept> // out_of_range
#include <utility>   // !=, <=, >, >=
#include <deque> // deque

using namespace std;

template<typename T, typename Container = vector<T> >
class MyStack {
	//Stack is LIFO 

	Container _c;

	/// - if you didn't use list initialization, will default construct
	/// 	_c and then use assignment operator on _c
	/// - using list initialization only uses copy constructor
	MyStack(const Container& c = Container()) : _c(c){}

	bool empty() {
		return _c.empty();
	}

	int size() {
		return _c.size();
	}

	T& top() {
		return _c.back(); // return back b/c stack is LIFO
	}

	void push(const T& v) {
		_c.push_back(v);
	}

	void pop() {
		_c.pop_back();
	}
};
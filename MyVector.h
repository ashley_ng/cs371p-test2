/// methods are based on what was tested in Vector1.c++

#include <algorithm> // equal, lexicographical_compare
#include <cstddef>   // ptrdiff_t, size_t
#include <memory>    // allocator
#include <stdexcept> // out_of_range
#include <utility>   // !=, <=, >, >=

using namespace std;

template <typename T>
class MyVector1 {
	/// DOES NOT USE ALLOCATOR
	T *_b; /// pointer to begining of vector
	T *_e; /// pointer to end of vector

	/// default constructor
	MyVector1(int size = 0, T& v = T()) {
		_b = new T[size]; /// uses T() size times
		_e = _b + size;
		fill(_b, _e, v); /// uses =T size times
	}
	/* all this gets replaced with the above constructor
	MyVector1() {
		_b = new T[0];
		_e = _b;
	}

	MyVector1*(int size) {
		_b = new T[size];
		_e = _b + size;
	}

	MyVector1(int size, T& v) {
		_b = new T[size];
		_e = _b + size;
		fill(_b, _e, v);
	}
	*/

	~MyVector1() {
		delete [] _b;
	}

	// --------------
	// index operator
	// --------------
	/// read/write access
	T& operator[](int index) {
		return _b[index]; /// pointer can act like array
	}

	/// read only access
	const T& operator[](int index) const {
		return _b[index];
	}

	// -----
	// begin
	// -----
	/// read/write access
	T* begin() {
		return _b;
	}
	///read only access
	const T* begin() const {
		return _b;
	}

	// ---
	// end
	// ---
	/// read/write access
	T* end() {
		return _e;
	}
	///read only access
	const T* end() const {
		return _e;
	}

	int size() {
		return _e - _b;
	}
};

/// default allocator
template <typename T, typename A = allocator<T>>
class MyVector2 {
	/// uses allocator
	T* _b;
	T* _e;
	A _a;

	MyVector2(int size = 0, const T& v = T(), const A& a = A()) {

	}
};